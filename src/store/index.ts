import axios from 'axios'
import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    listStatus: [{value: null, text: 'Seleccione...'}],
    listAreas: [{value: null, text: 'Seleccione...'}],
    listCountries: [{value: null, text: 'Seleccione...'}],
    listIdentificationTypes: [{value: null, text: 'Seleccione...'}],
    listEmployees: [],
    pages: 10,
    currentPage: 1,
    isEditing: false,
    showModal: false,
    employeeSelected: {
        id: null,
        identificationType: null,
        identification: null,
        firstSurname: null,
        secondSurname: null,
        firstName: null,
        otherNames: null,
        country: null,
        email: null,
        area: null,
        admissionDate: null
      }
  },
  getters: {
  },
  mutations: {
    setListStatus(state,payload){
      payload.data.forEach((el:any) => {
        state.listStatus.push(el)
      });
    },
    setListAreas(state,payload){
      payload.data.forEach((el:any) => {
        state.listAreas.push(el)
      });
    },
    setListCountries(state,payload){
      payload.data.forEach((el:any) => {
        state.listCountries.push(el)
      });
    },
    setListIdentificationTypes(state,payload){
      payload.data.forEach((el:any) => {
        state.listIdentificationTypes.push(el)
      });
    },
    setListEmployees(state,payload){
      state.listEmployees = payload.employees
      state.currentPage =  payload.currentPage
    },
    setEmployeeData(state,payload){
      state.employeeSelected = payload.data
    },
    resetEmployeeData(state){
      state.listStatus = [{value: null, text: 'Seleccione...'}],
      state.listAreas = [{value: null, text: 'Seleccione...'}],
      state.listCountries = [{value: null, text: 'Seleccione...'}],
      state.listIdentificationTypes = [{value: null, text: 'Seleccione...'}],
      state.employeeSelected = {
        id: null,
        identificationType: null,
        identification: null,
        firstSurname: null,
        secondSurname: null,
        firstName: null,
        otherNames: null,
        country: null,
        email: null,
        area: null,
        admissionDate: null
      }
    },
    setEditing(state,payload){
      state.isEditing = payload
    },
    toggleModal(state){
      state.showModal = !state.showModal
    }
  },
  actions: {
    getEmployees(store,page?:number){
      let url = `${process.env.VUE_APP_API_URL}/employees`
      if(page! > 1){
        url = `${process.env.VUE_APP_API_URL}/employees${page ? '?page='+page : ''}`
      }
      axios.get(url).then(response => {
        store.commit('setListEmployees',{
          employees: response.data.data,
          currentPage: !page ? 1 : page
        })
      })
    },
    getDataEmployee(store,id:string){
      let url = `${process.env.VUE_APP_API_URL}/employees/${id}`
      axios.get(url).then(response => {
        store.commit('setEmployeeData',response.data)
      })
    },
    setEditing(store,editing){      
      if(!editing){
        store.commit('resetEmployeeData') 
      }
      store.commit('setEditing',editing)
    },
    toggleModal(store){
      if(!store.state.showModal){
        store.dispatch('getListCountries')
        store.dispatch('getListAreas')
        store.dispatch('getListStatus')
        store.dispatch('getListIdentificationTypes')
      }
      store.commit('toggleModal')
    },
    getListCountries(store){
      let url = `${process.env.VUE_APP_API_URL}/masters/countries`
      axios.get(url).then(response => {
        store.commit('setListCountries',response.data)
      })
    },
    getListAreas(store){
      let url = `${process.env.VUE_APP_API_URL}/masters/areas`
      axios.get(url).then(response => {
        store.commit('setListAreas',response.data)
      })
    },
    getListStatus(store){
      let url = `${process.env.VUE_APP_API_URL}/masters/status`
      axios.get(url).then(response => {
        store.commit('setListStatus',response.data)
      })
    },
    getListIdentificationTypes(store){
      let url = `${process.env.VUE_APP_API_URL}/masters/identification-types`
      axios.get(url).then(response => {
        store.commit('setListIdentificationTypes',response.data)
      })
    },
    async deleteEmployee(store,id){
      let url = `${process.env.VUE_APP_API_URL}/employees/${id}`
      return axios.delete(url).then(response => {
        store.dispatch('getEmployees')
        return response.data.data
      })
    },    
    updateEmployee(store,data){
      let url = `${process.env.VUE_APP_API_URL}/employees/${data.id}`
      return axios.patch(url,data.employee).then(response => {
        store.dispatch('getEmployees')
        return response.data
      })
    },
    createEmployee(store,data){
      let url = `${process.env.VUE_APP_API_URL}/employees`
      return axios.post(url,data).then(response => {
        store.dispatch('getEmployees')
        return response.data
      })
    },
  },
  modules: {
  }
})
