# cidenet-front


Este proyecto corresponde al frontend del sistema que administra los empleados.

## Clonar el proyecto

```
git clone https://gitlab.com/arciortiz818/cidenet-front.git
cd cidenet-front
npm install
```

## Ejecutar en modo desarrollo
Para ejecutar el proyecto necesita crear el archivo .env en la raíz del proyecto y configurar las siguientes variables, para configurar la URL del API, por ejemplo:
```
VUE_APP_API_URL=http://localhost:4000/api/v1
```

Después de configurado el archivo .env, puede ejecutar el proyecto con el siguiente comando:
```
npm run serve
```

